/*! \file  testGraph2.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date November 20, 2015, 9:48 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! testGraph2 - */

/*!
 *
 */
void testGraph2(void)
{
  int i;
  
  clearScreen();
  TFTGinit();
  TFTGcolors(YELLOW,WHITE,CYAN,CYAN,BLUE,BLACK,RED);
  TFTGexpose();
  for ( i=0; i<278; i++ )
    TFTGaddPoint(i/2);

}
