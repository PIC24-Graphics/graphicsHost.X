/*! \file  testLines.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 3:46 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"



/*! testLines - */

/*!
 *
 */
void testLines(void)
{
  int i;
  int x1,x2,y1,y2;
  unsigned char r, g, b;


  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(100, 10, 12, "Test Lines");

  setColorX(SANDYBROWN);
  for ( i=-9; i<10; i++ )
    line( 160-10*i, 200, 160, 239 );
  setColorX(FORESTGREEN);
  for ( i=-9; i<10; i++ )
    line( 160-10*i, 200, 160, 161 );

  for ( i=0; i<20; i++ )
    {
      setColorRGB(128,50+10*i,150+5*i);
      line( 10+2*i,30,10+2*i,70);
    }

  for ( i=0; i<20; i++ )
    {
      setColorRGB(50+10*i,128,150+5*i);
      line( 10,80+2*i,50,80+2*i);
    }

  setColorX(WHITE);
  line (60,30,310,30);
  line(310,30,310,150);
  line(310,150,60,150);
  line(60,150,60,30);

  for (i = 0; i < 100; i++)
    {
      x1 = 61 + (int) ((248L * (long) rand()) / 32768L);
      y1 = 31 + (int) ((118L * (long) rand()) / 32768L);
      x2 = 61 + (int) ((248L * (long) rand()) / 32768L);
      y2 = 31 + (int) ((118L * (long) rand()) / 32768L);
      r = (unsigned char) ((255L * (long) rand()) / 32768L);
      g = (unsigned char) ((255L * (long) rand()) / 32768L);
      b = (unsigned char) ((255L * (long) rand()) / 32768L);
      setColorRGB(r,g,b);
      line(x1, y1, x2, y2);
    }
}
