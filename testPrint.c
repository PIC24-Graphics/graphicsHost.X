/*! \file  testPrint.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 15, 2015, 6:43 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! testPrint - */

/*!
 *
 */
void testPrint(void)
{
  int i;

  transitionOpen(BLACK);
  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(20, 11, 13, "Test Print/PrintInt");

  setColorX(GRAY20);
  for (i=69; i<233; i+=20)
    line(10,i,310,i);

  setFont(FONTSMALL);
  setColorX(TURQUOISE);
  putString(10,40,6,"putString(170,40,6,");
  putString(10,52,6,"\"Beware the Jabberwock\");");
  putString(10,72,6,"printInt(1234,0,220,72);");
  putString(10,92,6,"printInt(1234,1,220,92);");
  putString(10,112,6,"printInt(1234,2,220,112);");
  putString(10,132,6,"printInt(1234,4,220,132);");
  putString(10,152,6,"printDec(1234,1,220,152);");
  putString(10,172,6,"printDec(1234,4,220,172);");
  putString(10,192,6,"printDec(1234,6,220,192);");
  putString(10,212,6,"printDec(1234,0,220,212);");

  setColorX(LIGHTGOLDENROD);
  putString(170,40,6,"Beware the Jabberwock");
  printInt(1234,0,220,72);
  printInt(1234,1,220,92);
  printInt(1234,2,220,112);
  printInt(1234,4,220,132);
  printDec(1234,1,220,152);
  printDec(1234,4,220,172);
  printDec(1234,6,220,192);
  printDec(1234,0,220,212);

}
