/*! \file  positioningGrid.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 2:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"

/*! positioningGrid - Draw a faint grid */

/*!
 *
 */
void positioningGrid(void)
{
  int i;

  setColorRGB(8, 8, 8);
  for (i = 0; i < 32; i++)
    line(10 * i + 5, 0, 10 * i + 5, 239);
  for (i = 0; i < 24; i++)
    line(0, i * 10 + 5, 329, i * 10 + 5);
  setColorRGB(64, 64, 64);
  for (i = 1; i < 32; i++)
    line(10 * i, 0, 10 * i, 239);
  for (i = 1; i < 24; i++)
    line(0, i * 10, 329, i * 10);
  setColorRGB(112, 112, 112);
  line(0, 100, 319, 100);
  line(0, 200, 319, 200);
  line(100, 0, 100, 239);
  line(200, 0, 200, 239);
  line(300, 0, 300, 239);
}
