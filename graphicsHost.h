/*! \file  graphicsHost.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 1:53 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHICSHOST_H
#define	GRAPHICSHOST_H

#ifdef	__cplusplus
extern "C"
{
#endif

/* NOTE: Some TFT functions were implemented as true functions when
 *       they were multiply used.  Functions used only once are implemented
 *       as macros here.  The exception is screenClear() which is actually
 *       smaller as a macro.                                                  */
#define displayInitialize(fmt) {  putchSerial(TFTINIT);  putchSerial(fmt); idleSync(); }
#define rect(x1,y1,x2,y2) { putchSerial(TFTRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
#define roundRect(x1,y1,x2,y2) { putchSerial(TFTROUNDRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
#define filledRoundRect(x1,y1,x2,y2) { putchSerial(TFTFILLROUNDRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
#define filledCircle(x,y,r) { putchSerial(TFTFILLCIRCLE); sendWord(x); sendWord(y); sendWord(r); }
#define circle(x,y,r) { putchSerial(TFTCIRCLE); sendWord(x); sendWord(y); sendWord(r); }
#define printInt(val,fmt,x,y) { putchSerial(TFTPRINTINT); sendWord(val); sendWord(fmt); sendWord(x); sendWord(y); }
#define printDec(val,fmt,x,y) { putchSerial(TFTPRINTDEC); sendWord(val); sendWord(fmt); sendWord(x); sendWord(y); }
#define setColorRGB(r,g,b) { putchSerial(TFTSETCOLOR); putchSerial(r); putchSerial(g); putchSerial(b);}
#define setBackColorRGB(r,g,b) { putchSerial(TFTSETBACKCOLOR); putchSerial(r); putchSerial(g); putchSerial(b);}
#define clearScreen() { putchSerial(TFTCLEAR); }
#define transitionOpen(c) { putchSerial(TFTTRANSITIONOPEN); sendWord(c); }
#define transitionClose(c) { putchSerial(TFTTRANSITIONCLOSE); sendWord(c); }
#define TFTGinit() { putchSerial(TFTGINIT); }
#define TFTGrange(x1,x2,y1,y2) { putchSerial(TFTGRANGE); sendWord(x1); sendWord(x2); sendWord(y1); sendWord(y2); }
#define TFTGexpose() { putchSerial(TFTGEXPOSE); }
#define TFTGaddPoint(p) { putchSerial(TFTGADDPT); sendWord(p); }
#define TFTGcolors(t,a,x,y,s,f,l) { putchSerial(TFTGCOLORS); sendWord(t); sendWord(a); sendWord(x); sendWord(y); sendWord(s); sendWord(f); sendWord(l); }
//void TFTGcolors( unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int );
#define TFTGtitle(p) { graphingStrings(TFTGTITLE,p); }
#define TFTGXlabel(p) { graphingStrings(TFTGXLABEL,p); }
#define TFTGYlabel(p) { graphingStrings(TFTGYLABEL,p); }
#define TFTGnoErase() { putchSerial(TFTGNOERASE); }

void waitAbit( int );
void waitAshort( int );
/*! Initialize the UART */
void serialInitialize( int );
/*! Send a character over the serial port */
void putchSerial( unsigned char );
/*! Send a bunch of nulls to guarantee sync with terminal */
void idleSync(void);
/*! Send a 16 bit word to the terminal */
void sendWord( unsigned int );
/* Display a character on the terminal */
void putChar(int, int, char );
/*! Display a string on the terminal */
void putString( int, int, int, char* );
/*! Set the foreground color, 16-bit color */
void setColorX( unsigned int );
/*! Set the background color, 16-bit color */
void setBackColorX( unsigned int );
/*! Draw a line */
void line( int,int,int,int );
/*! Draw a filled rectangle */
void filledRect(int,int,int,int);
/*! Select the font to use */
void setFont(unsigned char);
/*! positioningGrid - Draw a faint grid */
void positioningGrid(void);
void putsTT( char * );
void graphingStrings(unsigned char, char *);

void testRGBcolors( void );
void test16bitColors( void );
void testFonts( void );
void testLines( void );
void testPixels( void );
void testRectangles( void );
void testRoundedRect( void );
void testCircles( void );
void testStringTT( void );
void testPrint( void );
void testGraph1(void);
void testGraph2(void);
void testGraph3(void);
void testGraph4(void);
void testGraph5(void);

#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSHOST_H */

