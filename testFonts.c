/*! \file  testFonts.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 3:23 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"



/*! testFonts - */

/*!
 *
 */
void testFonts(void)
{
  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(100, 10, 12, "Test Fonts");
  
  setColorX(DEEPSKYBLUE);

  setFont(FONTDJS);
  putString(200,  50, 8, "FONTSMALL");
  putString(200,  72, 8, "FONTBIG");
  putString(200, 100, 8, "FONTDJS");
  putString(200, 155, 8, "FONTSEVENSEGNUM");

  setColorX(GOLDENROD);

  setFont(FONTSMALL);
  putString(30,50,6,"The rain in Spain stays m");

  setFont(FONTBIG);
  putString(30,70,13,"The rain in");

  setFont(FONTDJS);
  putString(30,100,8,"The rain in Spain s");

  setColorX(RED);
  setFont(FONTSEVENSEGNUM);
  putString(30, 140, 34, "2468");

}
