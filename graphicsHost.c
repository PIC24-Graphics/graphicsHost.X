/*! \file  graphicsHost.c
 *
 *  \brief TFT graphics host
 *
 * Displays a series of screens showing many of the features of the
 * serial terminal.
 *
 *
 *  \author jjmcd
 *  \date June 1, 2015, 3:56 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

/* Baud rate generator calculations */
//! On-board Crystal frequency
#define XTFREQ          8000000
//! On-chip PLL setting
#define PLLMODE         2
//! Instruction Cycle Frequency
#define FCY             XTFREQ*PLLMODE/2
//! Desired baud rate
#define BAUDRATE         38400
//! Value for the baud rate generator
#define BRGVAL          ((FCY/BAUDRATE)/16)-1

#define WAITLEN 130
//#define NOTEST

const char szMessage[15][48] = {
 "Four score and seven years ago our",
 "fathers brought forth on this continent",
 "a new nation, conceived in Liberty, and",
 "dedicated to the proposition that all",
 "men are created equal.",
 "Now we are engaged in a great civil war",
 "testing whether that nation, or any",
 "nation so conceived and so dedicated,",
 "can long endure. We are met on a great",
 "battle-field of that war. We have come",
 "to dedicate a portion of that field, ",
 "as a final resting place for those who",
 "here gave their lives that that nation",
 "might live. It is altogether fitting",
 "and proper that we should do this."
};

const char  szMessage2[19][48] = {
  "When in the course of human events\r\n",
  "it becomes necessary for one people to\r\n",
  "dissolve the political bonds which\r\n",
  "have connected them to another, and\r\n",
  "to assume among the\r\n",
  "powers of the earth\r\n",
  "the separate and\r\n",
  "equal station to which the laws of\r\n",
  "nature and of nature's God entitle\r\n",
  "them, a decent respect to the opinions\r\n",
  "of mankind requires that they should\r\n",
  "declare the causes which impel them to\r\n",
  "the separation.\r\n",
  "We hold these truths to be\r\n",
  "self-evident, that all men are created\r\n",
  "equal, that they are endowed by their\r\n",
  "Creator with certain unalienable Rights,\r\n",
  "that among these are Life, Liberty and\r\n",
  "the pursuit of Happiness."
};

const char szMessage3[] = "AbCdEfGhIjKlMnOpQrStUvWxYzaBcDeFgHiJkLmNoPqRsTuVwXyZ0123456789!@#$%^&*()[]{}<>,.;'./";

const int nBars[20]= {
    190, 180, 160, 80, 140,
    55, 120, 100, 70, 120,
    145, 180, 160, 80, 140,
    40, 120, 100, 70, 120
};
const unsigned int nColorChoice[4] = {
        AQUAMARINE,
        GOLDENROD,
        SALMON,
        YELLOWGREEN };

int nColorSel;

/*! Wait a fairly long time */
void waitAbit( int len )
{
  int i,j;

  _LATB9 = 0;
  for ( j=0; j<len; j++ )
    for (i=0; i<32000; i++ )
        ;
  _LATB9 = 1;
}

/*! Wait a short time */
void waitAshort( int len )
{
  int i,j;

  for ( j=0; j<len; j++ )
    for (i=0; i<1000; i++ )
        ;
}

/*! graphingStrings - */

/*!
 *
 */
void graphingStrings(unsigned char opcode, char *p)
{
  int i;

  putchSerial(opcode);
  for ( i=0; i<23; i++ )
    {
      putchSerial( *p );
      p++;
    }
  putchSerial(0);
}

/*! Initialize the UART */
/*! serialInitialize() initializes UART1 to the selected baud rate and the
 *  desired set of pins.
 *
 * \param bAlternate int 0 to use default UART pins, 1 to use alternate pins
 * \returns none
 */
void serialInitialize( int bAlternate )
{
      // UART setup
    U1BRG  = BRGVAL;        // Set up baud rate generator
    if ( bAlternate )       // Alternate pins selected?
        U1MODE = 0x8400;    // Reset UART to 8-n-1, alt pins, and enable
    else
        U1MODE = 0x8000;    // Reset UART to 8-n-1, primary pins, and enable
    U1STA  = 0x0440;        // Reset status register and enable TX & RX
    _U1RXIF=0;              // Clear UART RX Interrupt Flag
}

/*! Send a character over the serial port */
/*! putchSerial() sends a single character over the serial port.
 *
 * \param ch unsigned char - character to send
 * \returns none
 */
void putchSerial( unsigned char ch )
{
    while(U1STAbits.UTXBF); // Wait if buffer full
    while(!U1STAbits.TRMT); // Wait for prev char to complete
    U1TXREG = ch;                   // char to UART xmit register
//    waitAshort(4);
}

/*! Send a bunch of nulls to guarantee sync with terminal */
void idleSync(void)
{
  int i;
  for ( i=0; i<20; i++ )
    {
      putchSerial(0);
      waitAshort(10);
    }
}

/*! Send a 16 bit word to the terminal */
void sendWord( unsigned int word )
{
  putchSerial(word&0xff);
  putchSerial((word&0xff00)>>8);
}

/* Display a character on the terminal */
void putChar(int x, int y, char ch )
{
      putchSerial(TFTPRINTCHAR);
      putchSerial(ch);
      sendWord(x);
      sendWord(y);
}

/*! Display a string on the terminal */
void putString( int x, int y, int delta, char *p )
{
  int xpos;
  xpos = x;

  while(*p)
    {
      putchSerial(TFTPRINTCHAR);
      putchSerial(*p);
      sendWord(xpos);
      sendWord(y);
      xpos+=delta;
      p++;
    }
}

/*! Set the foreground color, 16-bit color */
void setColorX( unsigned int color )
{
  putchSerial(TFTSETCOLORX);
  sendWord(color);
}

/*! Set the background color, 16-bit color */
void setBackColorX( unsigned int color )
{
  putchSerial(TFTSETBACKCOLORX);
  sendWord(color);
}

/*! Draw a line */
void line(int x1,int y1,int x2,int y2)
{
  putchSerial(TFTLINE);
  sendWord(x1);
  sendWord(y1);
  sendWord(x2);
  sendWord(y2);
}

/*! Draw a filled rectangle */
void filledRect(int x1,int y1,int x2,int y2)
{
  putchSerial(TFTFILLRECT);
  sendWord(x1);
  sendWord(y1);
  sendWord(x2);
  sendWord(y2);
}

/*! Select the font to use */
void setFont(unsigned char font)
{
  putchSerial(TFTSETFONT);
  putchSerial(font);
}

/*! main - Display a sample screen */

/*! Display a screen on the TFT terminal showing most of the TFT
 *  library features
 */
int main(void)
{
  int i;
#ifdef NOTEST
  int j;
#endif

  // LED will be used for general signaling
  _TRISB9 = 0;

  waitAbit(100);

  serialInitialize( 0 );

  // Sync
  for ( i=0; i<15; i++ )
    putchSerial(0);

  // Initialize TFT
  displayInitialize(TFTLANDSCAPE);

  nColorSel = 0;
      setBackColorX(BLACK);
      clearScreen();

  while(1)
    {

/*======================================================================*/
/*    First screen, RGBcolors                                           */
/*======================================================================*/

      testRGBcolors();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Second screen, 16-bit colors                                      */
/*======================================================================*/

      test16bitColors();
      waitAbit(WAITLEN);


/*======================================================================*/
/*    Third screen, Fonts                                               */
/*======================================================================*/

      testFonts();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Fourth screen, Pixels                                             */
/*======================================================================*/

      testPixels();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Fifth screen, Lines                                               */
/*======================================================================*/

      testLines();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Sixth screen, Rectangles                                          */
/*======================================================================*/

      testRectangles();
      waitAbit(WAITLEN);


/*======================================================================*/
/*    Seventh screen, Rounded Rectangles                                */
/*======================================================================*/
      testRoundedRect();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Eighth screen, Circles                                            */
/*======================================================================*/

      testCircles();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Ninth screen, TFTPRINT,PRINTINT                                   */
/*======================================================================*/

      testPrint();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Tenth screen, TFTSTRINGTT                                         */
/*======================================================================*/

      testStringTT();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Eleventh screen, Raw graph                                        */
/*======================================================================*/
      testGraph1();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Twelfth screen, Graph colors                                      */
/*======================================================================*/
      testGraph2();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Thirteenth screen, Graph labels                                   */
/*======================================================================*/
      testGraph3();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Fourteenth screen, Graph annotation                               */
/*======================================================================*/
      testGraph4();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Fifteenth screen, Multiple traces                                 */
/*======================================================================*/
      testGraph5();
      waitAbit(WAITLEN);

/*======================================================================*/
/*    Sixteenth screen, TFTSOFTWAREID                                   */
/*======================================================================*/

      putchSerial(0x23);
      waitAbit(WAITLEN);

      idleSync();

}

  return 0;
}
