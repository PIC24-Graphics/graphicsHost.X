/*! \file  testGraph5.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date November 20, 2015, 11:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"

#define TFTGMAXX 278
#define TFTGMAXY 190



/*! testGraph5 - */

/*!
 *
 */
void testGraph5(void)
{
  int i,y;
  
  clearScreen();
  TFTGinit();
  TFTGcolors(YELLOW,WHITE,CYAN,CYAN,NAVY,BLACK,LAWNGREEN);
  TFTGtitle("Multi-Trace");
  TFTGXlabel("X-label");
  TFTGYlabel("Y-label");
  TFTGrange(0,100,10,20);
  TFTGnoErase();
  TFTGexpose();
  for ( i=0; i<3*TFTGMAXX; i++ )
    {
      y=i%TFTGMAXY;
      TFTGaddPoint(y);
      if ( i==TFTGMAXX )
        TFTGcolors(YELLOW,WHITE,CYAN,CYAN,NAVY,BLACK,0xf863);
      if ( i==(2*TFTGMAXX) )
        TFTGcolors(YELLOW,WHITE,CYAN,CYAN,NAVY,BLACK,DODGERBLUE);
    }

}
