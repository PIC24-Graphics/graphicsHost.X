/*! \file  testStringTT.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 15, 2015, 3:55 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"

char szString1[]="Twas brillig, and the slithy toves Did gyre and\r\ngimble in the wabe: All mimsy were the borogoves, And\r\nthe mome raths outgrabe.";
char szString2[]="Twas brillig, and the\r\nslithy toves Did gyre and\r\ngimble in the wabe:";
char szString3[]="Twas brillig, and the slithy toves Did\r\ngyre and gimble in the wabe: All mimsy\r\nwere the borogoves, And the mome raths\r\noutgrabe.";



/*! testStringTT - */

/*!
 *
 */
void testStringTT(void)
{
  transitionClose(BLACK);
  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(50, 11, 13, "Test StringTT");

  putsTT("\r\n\n\n");
  setFont(FONTSMALL);
  setColorX(DODGERBLUE);
  putsTT(szString1);

  putsTT("\r\n\n");
  setFont(FONTBIG);
  setColorX(LAWNGREEN);
  putsTT(szString2);

  putsTT("\r\n\n");
  setFont(FONTDJS);
  setColorX(GOLD);
  putsTT(szString3);

}
