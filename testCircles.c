/*! \file  testCircles.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 15, 2015, 11:37 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"



/*! testCircles - */

/*!
 *
 */
void testCircles(void)
{
  int i,x1,y1,rad,vmin,vmax;
  unsigned char r,g,b;

  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorX(WHITE);
  setFont(FONTBIG);
  putString(90, 10, 12, "Test Circles");

  rect(10,120,310,230);

  setColorX(ORCHID);
  circle(50,60,30);
  setColorX(DEEPPINK);
  circle(80,70,40);

  setColorX(DARKSLATEBLUE);
  filledCircle(270,70,40);
  setColorX(DARKBLUE);
  filledCircle(240,75,20);
  setColorX(ROYALBLUE);
  filledCircle(220,80,10);

  for ( i=0; i<50; i++ )
    {
      rad = 5 + ((25L * (long) rand()) / 32768L);
      vmin = 11 + rad;
      vmax = 309 - rad;
      x1 = vmin + (int) ((((long)(vmax-vmin)) * (long) rand()) / 32768L);
      vmin = 121 + rad;
      vmax = 229 - rad;
      y1 = vmin + (int) ((((long)(vmax-vmin)) * (long) rand()) / 32768L);
      r = (unsigned char) ((255L * (long) rand()) / 32768L);
      g = (unsigned char) ((255L * (long) rand()) / 32768L);
      b = (unsigned char) ((255L * (long) rand()) / 32768L);
      setColorRGB(r,g,b);
      if ( (i&1) )
        {
          filledCircle(x1,y1,rad);
        }
      else
        {
          circle(x1,y1,rad);
        }
    }

}
