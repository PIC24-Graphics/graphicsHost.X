/*! \file  testRoundedRect.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 15, 2015, 11:16 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! testRoundedRect - */

/*!
 *
 */
void testRoundedRect(void)
{
  int i,x1,y1,x2,y2;
  unsigned char r,g,b;

  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorX(WHITE);
  setFont(FONTBIG);
  putString(20, 10, 12, "Test Rounded Rectangles");

  setColorX(LIGHTSEAGREEN);
  roundRect(10,50,80,100);
  setColorX(CORNFLOWERBLUE);
  roundRect(20,60,90,110);

  setColorX(SADDLEBROWN);
  filledRoundRect(10,130,80,180);
  setColorX(CHOCOLATE);
  filledRoundRect(20,140,90,190);

  setColorX(WHITE);
  rect( 150, 40, 310, 230 );

  for ( i=0; i<50; i++ )
    {
      x1 = 151 + (int) ((158L * (long) rand()) / 32768L);
      y1 = 41 + (int) ((188L * (long) rand()) / 32768L);
      x2 = 151 + (int) ((158L * (long) rand()) / 32768L);
      y2 = 41 + (int) ((188L * (long) rand()) / 32768L);
      r = (unsigned char) ((255L * (long) rand()) / 32768L);
      g = (unsigned char) ((255L * (long) rand()) / 32768L);
      b = (unsigned char) ((255L * (long) rand()) / 32768L);
      setColorRGB(r,g,b);
      if ( i & 1 )
        {
          roundRect(x1,y1,x2,y2);
        }
      else
        {
          filledRoundRect(x1,y1,x2,y2);
        }
    }

}
