/*! \file  testPixels.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 8:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


void pixel(int x, int y)
{
  putchSerial(TFTPIXEL);
  sendWord(x);
  sendWord(y);
}


/*! testPixels - */

/*!
 *
 */
void testPixels(void)
{
  int x,y;
  double theta,value;
  unsigned char r,g,b;

  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(100, 10, 12, "Test Pixels");

  for ( x=20; x<60; x++ )
    for ( y=30; y<70; y++ )
      {
        setColorX(rand());
        pixel(x,y);
      }
  for ( x=0; x<40; x++ )
    {
      theta = (double)x / 10.0;
      value = 100.0 + 20.0 * sin(theta);
      setColorX(rand());
      y = (int)value;
      pixel(x+20,y);
    }
  for ( x=0; x<25; x++ )
    for ( y=0; y<25; y++ )
      {
        r = g = b = (x+y)*5;
        setColorRGB(r,g,b);
        pixel(x+90,y+40);
      }
  for ( x=0; x<25; x++ )
    for ( y=0; y<25; y++ )
      {
        g = x * 10;
        b = y * 10;
        setColorRGB(255,g,b);
        pixel(x+130,y+40);
      }
  for ( x=0; x<25; x++ )
    for ( y=0; y<25; y++ )
      {
        g = x * 10;
        r = y * 10;
        setColorRGB(r,g,255);
        pixel(2*x+130,2*y+100);
      }
}
