/*! \file  testRGBcolors.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 2:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! testRGBcolors - */

/*!
 *
 */
void testRGBcolors(void)
{
  int i;

  //setBackColorRGB(0, 0, 0);
  //clearScreen();
  for ( i=40; i>=0; i-- )
    {
      setColorRGB((255/40)*i,0,0);
      rect(i,i,319-i,239-i);
      waitAbit(1);
    }
  transitionOpen(BLACK);

  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(70, 10, 12, "Test RGB colors");

  setFont(FONTDJS);

  setColorRGB(255, 0, 0);
  putString(90, 70, 9, "Red");
  setColorRGB(0, 255, 0);
  putString(90, 90, 9, "Green");
  setColorRGB(0, 0, 255);
  putString(90, 110, 9, "Blue");

  setColorRGB(0, 255, 255);
  putString(180, 70, 9, "Cyan");
  setColorRGB(255, 0, 255);
  putString(180, 90, 9, "Magenta");
  setColorRGB(255, 255, 0);
  putString(180, 110, 9, "Yellow");

  setFont(FONTSEVENSEGNUM);
  setColorRGB(128, 224, 255);
  putString(4, 150, 31, "0123456789");

  for (i = 0; i < 20; i++)
    {
      setColorRGB(255, 12 * i, 255);
      line(0, 60 - i, 400, 60 - i);
      setColorRGB(12 * i, 255, 255);
      line(0, 218 + i, 400, 218 + i);
    }
}
