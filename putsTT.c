/*! \file  putsTT.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 15, 2015, 4:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHost.h"

/*! putsTT - */

/*!
 *
 */
void putsTT(char *p)
{
  putchSerial(0x82);
  while ( *p )
    putchSerial( *p++ );
  putchSerial( 0x00 );
}
