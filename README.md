# Graphics Terminal Test

**graphicsHost.X** is a simple test program written for the
`PIC24FV32KA301`.  It sends commands to the graphics terminal to
exercise most of the features of the graphics terminal.

The program exercises a specific feature, then delays about two
seconds to allow the result to be viewed.  It takes about 30 
seconds to cycle through all the tests.

The PIC24 is expected to be connected through the serial port 
to the graphics terminal.  **graphicsHost.X** does not honor
X-on/X-off commands and expects the terminal to keep up (not
all that difficult given the delay between each test).


&nbsp;

## Tests run

- Set RGB color
- Set 16-bit color
- Select font
- Draw individual pixels
- Draw lines
- Draw rectangles
- Draw rounded rectangles
- Draw circles
- Test printing, showing numbers
- Test print teletype style
- Test unadorned graph
- Test graph with defined colors
- Test graph with labels
- Test graph with annotation
- Test graph with multiple passes
- Test graph with multiple passes and no erase
- Show software ID screen


&nbsp;

## Example screenshots

![Teletype Strings](images/AT-stringTT.jpg)  &nbsp; &nbsp;  ![16-bit colors](images/AT-16bitColor.jpg)

&nbsp;

&nbsp;

![Circles](images/AT-circle.jpg) &nbsp; &nbsp;  ![Rounded rectangles](images/AT-roundRect.jpg)

&nbsp;

&nbsp;

![Print/Print integers](images/AT-printInt.jpg) &nbsp; &nbsp;  ![Draw Lines](images/AT-lines.jpg)






