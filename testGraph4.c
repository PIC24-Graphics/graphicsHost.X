/*! \file  testGraph4.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date November 20, 2015, 10:14 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"

#define TFTGMAXX 278
#define TFTGMAXY 190

/*! testGraph4 - */

/*!
 *
 */
void testGraph4(void)
{
  int i,y;
  
  clearScreen();
  TFTGinit();
  TFTGcolors(YELLOW,WHITE,CYAN,CYAN,DARKGREEN,0x3820,TOMATO);
  TFTGtitle("Multi-Pass");
  TFTGXlabel("X-label");
  TFTGYlabel("Y-label");
  TFTGrange(0,100,10,20);
  TFTGexpose();
  for ( i=0; i<2*TFTGMAXX; i++ )
    {
      y=i%TFTGMAXY;
      TFTGaddPoint(y);
      waitAshort(7);
    }
}
