/*! \file  testGraph3.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date November 20, 2015, 9:55 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! testGraph3 - */

/*!
 *
 */
void testGraph3(void)
{
  int i;
  
  clearScreen();
  TFTGinit();
  TFTGcolors(YELLOW,WHITE,CYAN,CYAN,BLUE,BLACK,RED);
  TFTGtitle("Labels");
  TFTGXlabel("X-label");
  TFTGYlabel("Y-label");
  TFTGexpose();
  for ( i=0; i<278; i++ )
    TFTGaddPoint(i/2);

}
