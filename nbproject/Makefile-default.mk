#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=graphicsHost.c positioningGrid.c testRGBcolors.c test16bitColors.c testFonts.c testLines.c testPixels.c testRectangles.c testRoundedRect.c testCircles.c testStringTT.c putsTT.c testPrint.c testGraph1.c testGraph2.c testGraph3.c testGraph4.c testGraph5.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/graphicsHost.o ${OBJECTDIR}/positioningGrid.o ${OBJECTDIR}/testRGBcolors.o ${OBJECTDIR}/test16bitColors.o ${OBJECTDIR}/testFonts.o ${OBJECTDIR}/testLines.o ${OBJECTDIR}/testPixels.o ${OBJECTDIR}/testRectangles.o ${OBJECTDIR}/testRoundedRect.o ${OBJECTDIR}/testCircles.o ${OBJECTDIR}/testStringTT.o ${OBJECTDIR}/putsTT.o ${OBJECTDIR}/testPrint.o ${OBJECTDIR}/testGraph1.o ${OBJECTDIR}/testGraph2.o ${OBJECTDIR}/testGraph3.o ${OBJECTDIR}/testGraph4.o ${OBJECTDIR}/testGraph5.o
POSSIBLE_DEPFILES=${OBJECTDIR}/graphicsHost.o.d ${OBJECTDIR}/positioningGrid.o.d ${OBJECTDIR}/testRGBcolors.o.d ${OBJECTDIR}/test16bitColors.o.d ${OBJECTDIR}/testFonts.o.d ${OBJECTDIR}/testLines.o.d ${OBJECTDIR}/testPixels.o.d ${OBJECTDIR}/testRectangles.o.d ${OBJECTDIR}/testRoundedRect.o.d ${OBJECTDIR}/testCircles.o.d ${OBJECTDIR}/testStringTT.o.d ${OBJECTDIR}/putsTT.o.d ${OBJECTDIR}/testPrint.o.d ${OBJECTDIR}/testGraph1.o.d ${OBJECTDIR}/testGraph2.o.d ${OBJECTDIR}/testGraph3.o.d ${OBJECTDIR}/testGraph4.o.d ${OBJECTDIR}/testGraph5.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/graphicsHost.o ${OBJECTDIR}/positioningGrid.o ${OBJECTDIR}/testRGBcolors.o ${OBJECTDIR}/test16bitColors.o ${OBJECTDIR}/testFonts.o ${OBJECTDIR}/testLines.o ${OBJECTDIR}/testPixels.o ${OBJECTDIR}/testRectangles.o ${OBJECTDIR}/testRoundedRect.o ${OBJECTDIR}/testCircles.o ${OBJECTDIR}/testStringTT.o ${OBJECTDIR}/putsTT.o ${OBJECTDIR}/testPrint.o ${OBJECTDIR}/testGraph1.o ${OBJECTDIR}/testGraph2.o ${OBJECTDIR}/testGraph3.o ${OBJECTDIR}/testGraph4.o ${OBJECTDIR}/testGraph5.o

# Source Files
SOURCEFILES=graphicsHost.c positioningGrid.c testRGBcolors.c test16bitColors.c testFonts.c testLines.c testPixels.c testRectangles.c testRoundedRect.c testCircles.c testStringTT.c putsTT.c testPrint.c testGraph1.c testGraph2.c testGraph3.c testGraph4.c testGraph5.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FV32KA301
MP_LINKER_FILE_OPTION=,--script=p24FV32KA301.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/graphicsHost.o: graphicsHost.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphicsHost.o.d 
	@${RM} ${OBJECTDIR}/graphicsHost.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  graphicsHost.c  -o ${OBJECTDIR}/graphicsHost.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphicsHost.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/graphicsHost.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/positioningGrid.o: positioningGrid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/positioningGrid.o.d 
	@${RM} ${OBJECTDIR}/positioningGrid.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  positioningGrid.c  -o ${OBJECTDIR}/positioningGrid.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/positioningGrid.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/positioningGrid.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRGBcolors.o: testRGBcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRGBcolors.o.d 
	@${RM} ${OBJECTDIR}/testRGBcolors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRGBcolors.c  -o ${OBJECTDIR}/testRGBcolors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRGBcolors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRGBcolors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/test16bitColors.o: test16bitColors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/test16bitColors.o.d 
	@${RM} ${OBJECTDIR}/test16bitColors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  test16bitColors.c  -o ${OBJECTDIR}/test16bitColors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/test16bitColors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/test16bitColors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testFonts.o: testFonts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testFonts.o.d 
	@${RM} ${OBJECTDIR}/testFonts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testFonts.c  -o ${OBJECTDIR}/testFonts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testFonts.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testFonts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testLines.o: testLines.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testLines.o.d 
	@${RM} ${OBJECTDIR}/testLines.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testLines.c  -o ${OBJECTDIR}/testLines.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testLines.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testLines.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testPixels.o: testPixels.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testPixels.o.d 
	@${RM} ${OBJECTDIR}/testPixels.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testPixels.c  -o ${OBJECTDIR}/testPixels.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testPixels.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testPixels.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRectangles.o: testRectangles.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRectangles.o.d 
	@${RM} ${OBJECTDIR}/testRectangles.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRectangles.c  -o ${OBJECTDIR}/testRectangles.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRectangles.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRectangles.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRoundedRect.o: testRoundedRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRoundedRect.o.d 
	@${RM} ${OBJECTDIR}/testRoundedRect.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRoundedRect.c  -o ${OBJECTDIR}/testRoundedRect.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRoundedRect.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRoundedRect.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testCircles.o: testCircles.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testCircles.o.d 
	@${RM} ${OBJECTDIR}/testCircles.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testCircles.c  -o ${OBJECTDIR}/testCircles.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testCircles.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testCircles.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testStringTT.o: testStringTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testStringTT.o.d 
	@${RM} ${OBJECTDIR}/testStringTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testStringTT.c  -o ${OBJECTDIR}/testStringTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testStringTT.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testStringTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putsTT.o: putsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putsTT.o.d 
	@${RM} ${OBJECTDIR}/putsTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putsTT.c  -o ${OBJECTDIR}/putsTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putsTT.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putsTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testPrint.o: testPrint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testPrint.o.d 
	@${RM} ${OBJECTDIR}/testPrint.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testPrint.c  -o ${OBJECTDIR}/testPrint.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testPrint.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testPrint.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph1.o: testGraph1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph1.o.d 
	@${RM} ${OBJECTDIR}/testGraph1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph1.c  -o ${OBJECTDIR}/testGraph1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph2.o: testGraph2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph2.o.d 
	@${RM} ${OBJECTDIR}/testGraph2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph2.c  -o ${OBJECTDIR}/testGraph2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph2.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph2.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph3.o: testGraph3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph3.o.d 
	@${RM} ${OBJECTDIR}/testGraph3.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph3.c  -o ${OBJECTDIR}/testGraph3.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph3.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph3.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph4.o: testGraph4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph4.o.d 
	@${RM} ${OBJECTDIR}/testGraph4.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph4.c  -o ${OBJECTDIR}/testGraph4.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph4.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph4.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph5.o: testGraph5.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph5.o.d 
	@${RM} ${OBJECTDIR}/testGraph5.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph5.c  -o ${OBJECTDIR}/testGraph5.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph5.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph5.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/graphicsHost.o: graphicsHost.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphicsHost.o.d 
	@${RM} ${OBJECTDIR}/graphicsHost.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  graphicsHost.c  -o ${OBJECTDIR}/graphicsHost.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphicsHost.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/graphicsHost.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/positioningGrid.o: positioningGrid.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/positioningGrid.o.d 
	@${RM} ${OBJECTDIR}/positioningGrid.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  positioningGrid.c  -o ${OBJECTDIR}/positioningGrid.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/positioningGrid.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/positioningGrid.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRGBcolors.o: testRGBcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRGBcolors.o.d 
	@${RM} ${OBJECTDIR}/testRGBcolors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRGBcolors.c  -o ${OBJECTDIR}/testRGBcolors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRGBcolors.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRGBcolors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/test16bitColors.o: test16bitColors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/test16bitColors.o.d 
	@${RM} ${OBJECTDIR}/test16bitColors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  test16bitColors.c  -o ${OBJECTDIR}/test16bitColors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/test16bitColors.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/test16bitColors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testFonts.o: testFonts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testFonts.o.d 
	@${RM} ${OBJECTDIR}/testFonts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testFonts.c  -o ${OBJECTDIR}/testFonts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testFonts.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testFonts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testLines.o: testLines.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testLines.o.d 
	@${RM} ${OBJECTDIR}/testLines.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testLines.c  -o ${OBJECTDIR}/testLines.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testLines.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testLines.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testPixels.o: testPixels.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testPixels.o.d 
	@${RM} ${OBJECTDIR}/testPixels.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testPixels.c  -o ${OBJECTDIR}/testPixels.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testPixels.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testPixels.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRectangles.o: testRectangles.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRectangles.o.d 
	@${RM} ${OBJECTDIR}/testRectangles.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRectangles.c  -o ${OBJECTDIR}/testRectangles.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRectangles.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRectangles.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testRoundedRect.o: testRoundedRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testRoundedRect.o.d 
	@${RM} ${OBJECTDIR}/testRoundedRect.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testRoundedRect.c  -o ${OBJECTDIR}/testRoundedRect.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testRoundedRect.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testRoundedRect.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testCircles.o: testCircles.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testCircles.o.d 
	@${RM} ${OBJECTDIR}/testCircles.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testCircles.c  -o ${OBJECTDIR}/testCircles.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testCircles.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testCircles.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testStringTT.o: testStringTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testStringTT.o.d 
	@${RM} ${OBJECTDIR}/testStringTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testStringTT.c  -o ${OBJECTDIR}/testStringTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testStringTT.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testStringTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putsTT.o: putsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putsTT.o.d 
	@${RM} ${OBJECTDIR}/putsTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putsTT.c  -o ${OBJECTDIR}/putsTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putsTT.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putsTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testPrint.o: testPrint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testPrint.o.d 
	@${RM} ${OBJECTDIR}/testPrint.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testPrint.c  -o ${OBJECTDIR}/testPrint.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testPrint.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testPrint.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph1.o: testGraph1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph1.o.d 
	@${RM} ${OBJECTDIR}/testGraph1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph1.c  -o ${OBJECTDIR}/testGraph1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph1.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph2.o: testGraph2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph2.o.d 
	@${RM} ${OBJECTDIR}/testGraph2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph2.c  -o ${OBJECTDIR}/testGraph2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph2.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph2.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph3.o: testGraph3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph3.o.d 
	@${RM} ${OBJECTDIR}/testGraph3.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph3.c  -o ${OBJECTDIR}/testGraph3.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph3.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph3.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph4.o: testGraph4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph4.o.d 
	@${RM} ${OBJECTDIR}/testGraph4.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph4.c  -o ${OBJECTDIR}/testGraph4.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph4.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph4.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/testGraph5.o: testGraph5.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/testGraph5.o.d 
	@${RM} ${OBJECTDIR}/testGraph5.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  testGraph5.c  -o ${OBJECTDIR}/testGraph5.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/testGraph5.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/testGraph5.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf  -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -Wl,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHost.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
