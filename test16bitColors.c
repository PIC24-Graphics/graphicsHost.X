/*! \file  test16bitColors.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 14, 2015, 2:08 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFThost.h"
#include "colors.h"
#include "graphicsHost.h"


/*! test16bitColors - */

/*!
 *
 */
void test16bitColors(void)
{
  transitionClose(BLACK);
  // Clear the screen
  setBackColorRGB(0, 0, 0);
  clearScreen();
  //positioningGrid();
  setColorRGB(255, 255, 255);
  setFont(FONTBIG);
  putString(45, 10, 12, "Test 16-bit colors");

  setFont(FONTDJS);

  setColorX(DARKSALMON);
  putString(5, 40, 9, "DARKSALMON");
  setColorX(FIREBRICK);
  putString(5, 60, 9, "FIREBRICK");
  setColorX(INDIANRED);
  putString(5, 80, 9, "INDIANRED");
  setColorX(OLIVEDRAB);
  putString(5, 100, 9, "OLIVEDRAB");
  setColorX(YELLOWGREEN);
  putString(5, 120, 9, "YELLOWGREEN");
  setColorX(FORESTGREEN);
  putString(5, 140, 9, "FORESTGREEN");
  setColorX(LIMEGREEN);
  putString(5, 160, 9, "LIMEGREEN");
  setColorX(SADDLEBROWN);
  putString(5, 180, 9, "SADDLEBROWN");
  setColorX(BURLYWOOD);
  putString(5, 200, 9, "BURLYWOOD");
  setColorX(SANDYBROWN);
  putString(5, 220, 9, "SANDYBROWN");

  setColorX(MEDIUMSEAGREEN);
  putString(115, 40, 9, "MEDIUMSEAGREEN");
  setColorX(LIGHTSEAGREEN);
  putString(115, 60, 9, "LIGHTSEAGREEN");
  setColorX(AQUAMARINE);
  putString(115, 80, 9, "AQUAMARINE");
  setColorX(SKYBLUE);
  putString(115, 100, 9, "SKYBLUE");
  setColorX(ROYALBLUE);
  putString(115, 120, 9, "ROYALBLUE");
  setColorX(DODGERBLUE);
  putString(115, 140, 9, "DODGERBLUE");
  setColorX(CORNFLOWERBLUE);
  putString(115, 160, 9, "CORNFLOWERBLUE");
  setColorX(PALEVIOLETRED);
  putString(115, 180, 9, "PALEVIOELETRED");
  setColorX(DARKMAGENTA);
  putString(115, 200, 9, "DARKMAGENTA");
  setColorX(DARKVIOLET);
  putString(115, 220, 9, "DARKVIOLET");

  setColorX(PINK);
  putString(255, 40, 9, "PINK");
  setColorX(PERU);
  putString(255, 60, 9, "PERU");
  setColorX(GOLD);
  putString(255, 80, 9, "GOLD");
  setColorX(YELLOW);
  putString(255, 100, 9, "YELLOW");
  setColorX(ORANGE);
  putString(255, 120, 9, "ORANGE");
  setColorX(SIENNA);
  putString(255, 140, 9, "SIENNA");
  setColorX(TAN);
  putString(255, 160, 9, "TAN");
  setColorX(WHITE);
  putString(255, 180, 9, "WHITE");
  setColorX(PLUM);
  putString(255, 200, 9, "PLUM");
  setColorX(VIOLET);
  putString(255, 220, 9, "VIOLET");
}
